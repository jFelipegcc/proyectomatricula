package mx.edu.uteq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ejemplo1SmvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ejemplo1SmvcApplication.class, args);
	}

}
