
package mx.edu.uteq.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class ControladorInicio {
    //@RequestMapping("/")
    @GetMapping("/") //solo queremos  peticiones get
    public String page(Model model) {
       // model.addAttribute("attribute", "value");
        return "index";        
    }
    //@RequestMapping("/otro")
    @GetMapping("/otra_pagina") //solo queremos  peticiones get
    public String otraPage(Model model) {
        //Persona per1 = new Persona;
       
       // model.addAttribute("attribute", "value");
       String[] datos = {"dato1","dato2", "dato3"};
       model.addAttribute ("dato","dato");
       model.addAttribute ("datos",datos);
       
        return "otro_view";
        
    }
    
}
